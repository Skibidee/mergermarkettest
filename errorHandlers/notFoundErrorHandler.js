"use strict";
// Libraries
var path = require("path");
// Models
// Services
// Other
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));

module.exports = function (err, req, res, next) {
    // Specific error handler for not found
    if (err.status === 404){
        return res.status(err.status)
        .jsonp({
            error: { 
                userMessage: userMessaging.notFoundError,
                errorStatus: err.status
            }
        });
    }
    return next(err);
}