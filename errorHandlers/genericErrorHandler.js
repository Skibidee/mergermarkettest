"use strict";
// Libraries
var path = require("path");
// Models
// Services
// Other
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));

module.exports = function (err, req, res, next) {
    return res.status(err.status || 500)
    .jsonp({
        error: { 
            userMessage: err.userMessage || userMessaging.genericServerError,
            errorStatus: err.status || 500            
        }
    });
};