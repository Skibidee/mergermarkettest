"use strict";
// Libraries
var path = require('path');
// Models
// Services
var systemLogger = require(path.join(__base, 'libs', 'logging', 'systemLogger.js'));
// Other

module.exports = function (err, req, res, next) {
    systemLogger.logError(err);
    return next(err);
};