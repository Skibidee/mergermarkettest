// base route - yes global, this is the best way I have found without using modules, environment variables or symlinks
global.__base = __dirname + '/';

// modules
var express = require('express');
var app = express();
var server = require('http').createServer(app);  
var bodyParser = require('body-parser');
var path = require('path');

var mongoose = require('mongoose');

var config = require(path.join(__base, 'config', 'config.js'));

// Controllers
var companyCardController = require(path.join(__base, 'controllers', 'companyCard.js'));

// BasicError
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));

// Error handlers
var errorLogger = require(path.join(__base, 'errorHandlers', 'errorLogger.js'));
var genericErrorHandler = require(path.join(__base, 'errorHandlers', 'genericErrorHandler.js'));
var notFoundErrorHandler = require(path.join(__base, 'errorHandlers', 'notFoundErrorHandler.js'));

/**
 * Connect to Mongoose DB
 */
var mongooseIntervalId = undefined;
mongooseConnect(); // connect to database

// When successfully connected
mongoose.connection.on('connected', function ()
{
    console.log('Mongoose default connection open');
    clearInterval(mongooseIntervalId);
    mongooseIntervalId = undefined;
});

// If the connection throws an error
mongoose.connection.on('error', function (err)
{
    console.log('Mongoose default connection error: ' + err);
    mongoose.disconnect();
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function ()
{
    console.log('Mongoose default connection disconnected');
    if (!mongooseIntervalId)
    {
        mongooseIntervalId = setInterval(mongooseConnect, 5000);
    }
});

// Use mongoose to connect to MongoDB.
function mongooseConnect()
{
    console.log("Connecting to MongoDB " + config.mongo.url);
    mongoose.connect(config.mongo.url, config.mongo.params);
}

// Blanket CORS middleware
var allowCrossDomain = function(req, res, next) {
    var oneof = false;
    if(req.headers.origin) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        oneof = true;
    }
    if(req.headers['access-control-allow-origin']) {
        res.header('Access-Control-Allow-Origin', req.headers['access-control-allow-origin']);
        oneof = true;
    }
    if(req.headers['access-control-request-method']) {
        res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
        oneof = true;
    }
    if(req.headers['access-control-request-headers']) {
        res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
        oneof = true;
    }
    if(oneof) {
        res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    }

    // intercept OPTIONS method
    if (oneof && req.method == 'OPTIONS') {
        return res.sendStatus(200);
    }
    else {
        return next();
    }
};

// Configure app to use bodyParser()
app.use(bodyParser.json());
// Allow cross domain
app.use(allowCrossDomain);


var router = express.Router();

// test route to make sure everything is working (accessed at GET http://URL:PORT/api)
router.route('/companyCards')
    .get(companyCardController.get);

// Register Routes under the base /api route
app.use('/api', router);

// This route is fired if there is no previous route that matches the request, it then 404's it which is picked up by the Not Found error handler
// must be after all others routes (but before error handling)
app.use('*', function(req, res, next) {
    var err = new BasicError("not_found", req.originalUrl + " - Not Found", 404);
    err.stack = null;
    //err.status = 404;
    next(err);
});

// Error handlers (in priority order)
app.use(errorLogger);
app.use(notFoundErrorHandler);
app.use(genericErrorHandler);

// START THE SERVER
var servery = server.listen(config.serverPort, function() {
    console.log('Server started on port ' + config.serverPort);
    console.log('Config Loaded: ' + JSON.stringify(config, null, '\t'));
});