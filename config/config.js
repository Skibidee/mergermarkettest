module.exports = {
    serverPort: 8080,
    mongo: {
        url: 'ds037551.mongolab.com:37551/mm-recruitment',
        params:  { user: 'mm_recruitment_user_readonly', pass: 'rebelMutualWhistle', server: { auto_reconnect: true } }
    },
    stockPriceEndpoint: "http://mm-recruitment-stock-price-api.herokuapp.com/company/",
    positiveWords: ["positive", "success", "grow", "gains", "happy", "healthy"],
    negativeWords: ["disappointing", "concerns", "decline", "drag", "slump", "feared"]
};
