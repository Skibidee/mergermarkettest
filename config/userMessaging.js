var path = require('path');
var config = require(path.join(__base, 'config', 'config.js'));

module.exports = {
	genericServerError: "It looks like something went wrong. Please try again",
	notFoundError: "It looks like something went wrong. Please try again",
	
	testApiResponse: "Welcome to the api, we've got fun and games"
};