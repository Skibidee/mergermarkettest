// project routes
global.__base = '../';

"use strict";
// Libraries
var path = require('path');
var assert = require('assert');
// Models
// Services
var emailChecker = require(path.join(__base, 'libs', 'extensions', 'emailChecker.js'));
// Other

describe('emailChecker', function() {
	describe('#isEmail()', function() {
		it('should return false when not an email', function() {
			assert.equal(false, emailChecker.isEmail("NOTANEMAIL"));
			assert.equal(true, emailChecker.isEmail("this@this.com"));
			assert.equal(false, emailChecker.isEmail("this@this."));
		});
	});
	describe('#isEmail()', function() {
		it('should return false when not an email', function() {
			assert.equal(false, emailChecker.isEmail("NOTANEMAIL"));
			assert.equal(true, emailChecker.isEmail("this@this.com"));
			assert.equal(false, emailChecker.isEmail("this@this."));
		});
	});
});

describe('emailChecker', function() {
	describe('#isEmail()', function() {
		it('should return false when not an email', function() {
			assert.equal(false, emailChecker.isEmail("NOTANEMAIL"));
			assert.equal(true, emailChecker.isEmail("this@this.com"));
			assert.equal(false, emailChecker.isEmail("this@this."));
		});
	});
});