"use strict";
// Libraries
var path = require('path');
var expect = require('chai').expect;
var sinon = require('sinon');
var rewire = require('rewire');
// Models
// Services
// Other

// project route
global.__base = path.join(__dirname, '..');

describe('newsFeedService', function() {

	describe('#getCompanyNewsFeed()', function() {
		var newsFeedService, requestStub;
		before(function(){
			requestStub = sinon.stub();			
			newsFeedService = rewire(path.join(__base, 'libs', 'services', 'newsFeedService.js'));
			newsFeedService.__set__({
				config: {
					positiveWords: ["positive", "success", "grow", "gains", "happy", "healthy"],
					negativeWords: ["disappointing", "concerns", "decline", "drag", "slump", "feared"]
				},
				request: requestStub
			})
		});		
		it('should return correct occurances count', function(done) {
			var mockFeeds = [
			{
				id: 74,
				headline: 'Google going strong, but maybe not for long.',
				body: 'Google has some concerns to address the balance of this year, and beyond. Over the long run, the consensus analyst recommendation for Google as a \'strong buy\' is warranted as the company continues driving a healthy double-digit top line growth. But that doesn\'t mean there won\'t be a hurdle, or three, to overcome along the way.',
			}, {
				id: 141,
				headline: 'Ad revenues still primary source of Google revenue.',
				body: 'Investors were encouraged by a healthy gain in the number of people looking at Google\'s ads, even as the average prices for those marketing messages extended a three-and-half year slump. The market also had been bracing for more disappointing numbers, triggering a \'relief rally\' when the results weren\'t as bad as feared, BGC Partners analyst Colin Gillis said.'
			}, {
				id: 121,
				headline: 'Blah blah news',
				body: 'positive, success, grow, gains, "happy", "healthydisappointing, "concerns, "decline", "drag "slump", "feared',
			}, {
				id: 454,
				headline: 'Blah blah more news',
				body: 'positivepositivepositivepositvepositivepositivepositivepostivepositivepositive',
			}];
			requestStub.yields(null, null, mockFeeds);
			newsFeedService.getCompanyNewsFeed("SOMEURL", function(err, newsArticles){
				expect(newsArticles[0].positivityRating).to.equal(1);
				expect(newsArticles[1].positivityRating).to.equal(-2);
				expect(newsArticles[2].positivityRating).to.equal(0);
				expect(newsArticles[3].positivityRating).to.equal(8);
				done();			
			});
			
		});
	});
	
	describe('#countStringOccurances()', function() {
		var newsFeedService;
		before(function(){
			newsFeedService = rewire(path.join(__base, 'libs', 'services', 'newsFeedService.js'));
		});		
		it('should return correct occurances count', function(done) {	
			var countStringOccurances = newsFeedService.__get__("countStringOccurances");
			expect(countStringOccurances("positive", "positivepositivepositive")).to.equal(3);
			expect(countStringOccurances("positive", "positiveposepositive")).to.equal(2);
			expect(countStringOccurances("positive", "positivositive")).to.equal(0);
			expect(countStringOccurances("positiv", "positivepositivepositive")).to.equal(3);
			expect(countStringOccurances("i", "positivepositivepositive")).to.equal(6);
			done();
		});
	});

	// This test is a little pointless, but is here as proof of testing private methods through stubbing and mocking using the rewire method
	describe('#getPositivityRating()', function() {
		var newsFeedService, countWordOccurancesByCategoryStub;
		before(function(){
			countWordOccurancesByCategoryStub = sinon.stub();
			newsFeedService = rewire(path.join(__base, 'libs', 'services', 'newsFeedService.js'));
			newsFeedService.__set__({
				config: {
					positiveWords: ["positive", "success", "grow", "gains", "happy", "healthy"],
					negativeWords: ["disappointing", "concerns", "decline", "drag", "slump", "feared"]
				},
				countWordOccurancesByCategory: countWordOccurancesByCategoryStub
			})
		});		
		it('should return positive count if more positive words are found', function(done) {	
			countWordOccurancesByCategoryStub.returns({positive: 10, negative: 5});
			var positivityCount = newsFeedService.__get__("getPositivityRating")("ANYTHING");
			expect(positivityCount).to.equal(5);
			done();
		});
		it('should return negative count if more negative words are found', function(done) {	
			countWordOccurancesByCategoryStub.returns({positive: 5, negative: 10});
			var positivityCount = newsFeedService.__get__("getPositivityRating")("ANYTHING");
			expect(positivityCount).to.equal(-5);
			done();
		});
		it('should return 0 if words are equal', function(done) {	
			countWordOccurancesByCategoryStub.returns({positive: 5, negative: 5});
			var positivityCount = newsFeedService.__get__("getPositivityRating")("ANYTHING");
			expect(positivityCount).to.equal(0);
			done();
		});
	});

});