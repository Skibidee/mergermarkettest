"use strict";
// Libraries
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// Models
// Services
// Other

var SystemLogSchema = new Schema({
    timestamp: {
        type: Date,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    topic: {
        type: String,
        required: true
    },
    verbosity: {
        type: String,
        required: true
    },
    stackTrace: {
        type: String
    },
    __v: { 
        type: Number, 
        select: false
    }
});

module.exports = mongoose.model('SystemLog', SystemLogSchema);