"use strict";
// Libraries
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// Models
// Services
// Other

var CompanySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    tickerCode: {
        type: String,
        required: true
    },
    __v: { 
        type: Number, 
        select: false
    }
}, { collection: 'company' });

module.exports = mongoose.model('Company', CompanySchema);