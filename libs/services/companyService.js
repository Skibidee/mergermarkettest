// Libraries
var path = require("path");
// Models
var Company = require(path.join(__base, 'models', 'company.js'));
// Services
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));

function getCompanies(callback){
	Company.find(function(err, companies){
		if (err){
			return callback(err);
		}
		return callback(null, companies);
	});
};

module.exports.getCompanies = getCompanies;