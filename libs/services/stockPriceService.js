// Libraries
var path = require("path");
var request = require("request");
// Models
// Services
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));
var config = require(path.join(__base, 'config', 'config.js'));

function getCompanyStockPrice(companyTickerCode, callback){
	var options = {
	    url: config.stockPriceEndpoint + companyTickerCode,
	    method: 'GET',
	    json: true
	}
	request(options, function(err, response, body){
		if (err){
			return callback(err);
		}
		return callback(null, body);
	});
};

module.exports.getCompanyStockPrice = getCompanyStockPrice;