// Libraries
var path = require("path");
var request = require("request");
var _ = require("lodash");
// Models
// Services
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));
var config = require(path.join(__base, 'config', 'config.js'));

function getCompanyNewsFeed(feedUrl, callback){
	var options = {
	    url: feedUrl,
	    method: 'GET',
	    json: true
	}
	request(options, function(err, response, body){
		if (err){
			return callback(err);
		}
		var newsArticles = _.map(body, function(article){
			article.positivityRating = getPositivityRating(article.body);
			return article;
		});
		return callback(null, newsArticles);
	});
};

function getPositivityRating(articleBody){
	var words = {
		positive: config.positiveWords,
		negative: config.negativeWords
	}
	var counts = countWordOccurancesByCategory(words, articleBody);
	return counts.positive - counts.negative;
}

function countWordOccurancesByCategory(categorisedWords, text){
	var counts = {};
	for (var category in categorisedWords) {
	    if (!categorisedWords.hasOwnProperty(category)) continue;
	    counts[category] = countWordsInText(categorisedWords[category], text);
	}
	return counts;
}

function countWordsInText(words, text) {    
    var count = 0;
    for (var i = words.length - 1; i >= 0; i--) {
    	var word = words[i];
    	count += countStringOccurances(word, text);
    };
    return count;    
}

function countStringOccurances(subString, string) {
    var count = 0;
    var position = 0;
    while (true) {
        position = string.indexOf(subString, position);
        if (position >= 0) {
            count++;
            position++;
        } else break;
    }
    return count;
}

module.exports.getCompanyNewsFeed = getCompanyNewsFeed;