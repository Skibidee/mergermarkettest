// Libraries
var path = require("path");
// Models
// Services
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));

function getApiMessage(language, callback){
    return callback(null, userMessaging.message("testApiResponse", language));
};

module.exports.getApiMessage = getApiMessage;