"use strict";
// Libraries
var util = require('util');
// Models
// Services
// Other

function BasicError(errorType, errorMessage, statusCode, errorTopic, userMessage) {
    
    /*INHERITANCE*/
    Error.call(this); //super constructor
    Error.captureStackTrace(this, this.constructor); //super helper method to include stack trace in error object

    this.errorType = errorType;
    this.errorMessage = errorMessage;
    this.status = statusCode;
    this.errorTopic = errorTopic;
    this.userMessage = userMessage;
}
 
// inherit from Error
util.inherits(BasicError, Error);

//Export the constructor function as the export of this module file.
module.exports = BasicError;