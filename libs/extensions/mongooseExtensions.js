"use strict";
// Libraries
var path = require('path');
var async = require('async');
// Models
// Services
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));

function bulkSaveAndRemoveOnError(schema, items, callback){
	var insertedIds = [];
	async.eachSeries(items, function(item, done){
		// Ensure this item is actually an instance of the schema, if not error
		if (!(item instanceof schema)){
			return done(new BasicError("item_is_not_instance_of_schema", "Attempting to bulk save an item with a mismatched schema", 500, "bulkSaveAndRemoveOnError"))
		}
		item.save(function(err){
			if (err){				
				return done(err);
			}
			insertedIds.push(item._id);			
			return done();
		});	
	}, function(err){
		if (err) {
			schema.remove({ _id: { $in: insertedIds } }, function(removalErr){
				if (removalErr){
					return callback(removalErr);
				}
				return callback(err);
			});
		} else {
			return callback();
		}
	});
};

module.exports.bulkSaveAndRemoveOnError = bulkSaveAndRemoveOnError;