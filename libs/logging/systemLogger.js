"use strict";
// Libraries
var path = require('path');
var moment = require('moment');
// Models
var SystemLog = require(path.join(__base, 'models', 'systemLog.model.js'));
// Services
// Other

function log (systemLog){
	console.log(systemLog);
};

function logError (err){
	var systemErrorLog = new SystemLog({
		verbosity: "Error",
		type: err.errorType || err.name,
		message: err.errorMessage || err.message,
		topic: err.errorTopic,
		stackTrace: err.stack,
		timestamp: moment.utc()
	});
	return log(systemErrorLog);
};

function logInfo (message, type, topic){
	var systemInfoLog = new SystemLog({
		verbosity: "Info",
		type: type,
		message: message,
		topic: topic,
		timestamp: moment.utc()
	});
	return log(systemInfoLog);
};

module.exports.logError = logError;
module.exports.logInfo = logInfo;