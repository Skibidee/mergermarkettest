// Libraries
var path = require('path');
// Models
// Services
var testService = require(path.join(__base, 'libs', 'services', 'testService.js'));
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));

function get(req, res, next){
	testService.getApiMessage(req.language, function(err, apiMessage){
		if (err){
			return next(new BasicError("error_type", "error message", 500, "error topic", "user error message"));
		}
		return res.status(200).jsonp({userMessage: apiMessage});		
	});	
};

module.exports.get = get;