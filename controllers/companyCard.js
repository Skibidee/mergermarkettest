// Libraries
var path = require('path');
var async = require('async');
// Models
// Services
var companyService = require(path.join(__base, 'libs', 'services', 'companyService.js'));
var newsFeedService = require(path.join(__base, 'libs', 'services', 'newsFeedService.js'));
var stockPriceService = require(path.join(__base, 'libs', 'services', 'stockPriceService.js'));
// Other
var BasicError = require(path.join(__base, 'libs', 'errors', 'basicError.js'));
var userMessaging = require(path.join(__base, 'config', 'userMessaging.js'));

function get(req, res, next){
	companyService.getCompanies(function(err, companies){
		if (err){
			return next(err);			
		}
		var companyReturnData = [];
		async.each(companies, function(company, asyncCallback){
			stockPriceService.getCompanyStockPrice(company.tickerCode, function(err, stockPrice){
				if (err){
					return asyncCallback(err);
				}
				var companyData = {
					name: company.name,
					tickerCode: company.tickerCode,
					stockPrice: stockPrice.latestPrice || 0,
					stockPriceUnits: stockPrice.priceUnits || "Unknown"
				}
				if (!stockPrice.storyFeedUrl){
					companyReturnData.push(companyData);
					return asyncCallback();
				}
				newsFeedService.getCompanyNewsFeed(stockPrice.storyFeedUrl, function(err, newsFeed){
					if (err){
						return asyncCallback();
					}
					companyData.newsFeed = newsFeed;
					companyReturnData.push(companyData);
					return asyncCallback();
				});
			});
		}, function(err){
			if (err){
				return next(err);
			}
			return res.status(200).jsonp(companyReturnData);
		});
	});
};

module.exports.get = get;